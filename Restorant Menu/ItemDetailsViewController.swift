//
//  ItemDetailsViewController.swift
//  Restorant Menu
//
//  Created by Alek Ivanovski on 6/24/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var priceView: UILabel!
    
    var item: ListItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainImage.image = item?.mainImage
        titleView.text = item?.title
        descriptionView.text = item?.itemDescription
        priceView.text = String(item!.price) + " MKD"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

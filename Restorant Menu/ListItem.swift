//
//  ListItem.swift
//  Restorant Menu
//
//  Created by Alek Ivanovski on 6/24/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//
import UIKit
import Foundation

class ListItem {
    
    var mainImage: UIImage
    var title: String
    var itemDescription: String
    var price: String
    
    init(mainImage: UIImage, title: String, description: String, price: String) {
        self.mainImage = mainImage
        self.itemDescription = description
        self.title = title
        self.price = price
    }
    
}

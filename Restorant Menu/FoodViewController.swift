//
//  FoodViewController.swift
//  Restorant Menu
//
//  Created by Alek Ivanovski on 6/23/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data = [ListItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        data = [ListItem.init(mainImage: #imageLiteral(resourceName: "MONSTER-BURGER.jpg"), title: "Monster Burger", description: "Whole wheat bread, 2 beef burger patties, mozzarella, lettuce, pickles, low fat sauce, ketchup, mixed salad.", price: "280"),
                ListItem.init(mainImage: #imageLiteral(resourceName: "AMERICAN-PROTEIN-PANCAKE"), title: "American Protein Cake", description: "American whole wheat pancake, chocolate whey spread and peanut butter, forest fruits.", price: "190"),
                ListItem.init(mainImage: #imageLiteral(resourceName: "PROTEIN-CHIA-PUDDING"), title: "Protein Chia Pudding", description: "Milk, banana, 30 g whey protein, chia seeds, mixed forest fruits, honey, almonds.", price: "190")]
        
        tableView.delegate = self
        tableView.dataSource = self
    }

}

extension FoodViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell") as! CustomCell
        cell.setMenuItem(item: item)
        print("cellForRowAt \(cell.mainImageView.image!)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "foodSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ItemDetailsViewController {
            destination.item = data[(tableView.indexPathForSelectedRow?.row)!]
        }
        tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }
}

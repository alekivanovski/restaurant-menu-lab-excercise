//
//  DrinksViewController.swift
//  Restorant Menu
//
//  Created by Alek Ivanovski on 6/23/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class DrinksViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var data = [ListItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = [ListItem.init(mainImage: #imageLiteral(resourceName: "Green-Power-Smoothie-Recipe-1-1200.jpg"), title: "Hulk", description: "Avocado, broccoli, spinach, apple, banana.", price: "120"),
                ListItem.init(mainImage: #imageLiteral(resourceName: "berry-banana-smoothie-horiz-a-1800"), title: "Banana Berry", description: "Low fat milk, banana, raspberries.", price: "120"),
                ListItem.init(mainImage: #imageLiteral(resourceName: "Mango-Orange-Green-Smoothie"), title: "Green Orange", description: "Orange, apple, carrot.", price: "130")]
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension DrinksViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "drinksCell") as! CustomDrinksCell
        cell.setMenuItem(item: item)
        print("cellForRowAt \(cell.mainImageView.image!)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "drinksSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ItemDetailsViewController {
            destination.item = data[(tableView.indexPathForSelectedRow?.row)!]
        }
        tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }
}

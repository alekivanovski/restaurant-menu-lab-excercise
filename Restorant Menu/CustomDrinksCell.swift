//
//  CustomDrinksCell.swift
//  Restorant Menu
//
//  Created by Alek Ivanovski on 6/25/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class CustomDrinksCell: UITableViewCell {
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var priceView: UILabel!
    

    func setMenuItem(item: ListItem) {
        mainImageView.image = item.mainImage
        titleView.text = item.title
        descriptionView.text = item.itemDescription
        priceView.text = item.price
        print(item.mainImage)
    }

}
